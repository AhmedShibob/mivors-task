import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    authenticated: false,
    locale: false
  },
  mutations: {
    setAuthentication(state, status) {
      state.authenticated = status;
    },
    changeLocale(state, status) {
      state.locale = status;
    }
  },
  actions: {},
  modules: {}
});
