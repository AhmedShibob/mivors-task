import Vue from "vue";
import VueI18n from "vue-i18n";
Vue.use(VueI18n);

const messages = {
  en: {
    logout: "Logout",
    projects: "Projects",
    sign_in: "Sign in",
    Email: "Email",
    Password: "Password",
    Please_input_password: "Please input password",
    Please_input_email_address: "Please input email address",
    Please_input_correct_email_address: "Please input correct email address",
    Congrats_you_sign_in_successful: "Congrats, you sign in successful.",
    Clean_Data: "Clean Data",
    Project_Name: "Project Name",
    Project_Number: "Project Number",
    Project_Describtion: "Project Describtion",
    Create_BOQ: "Create BOQ",
    BOQ_Version: "BOQ Version",
    BOQ_Budget_Amount: "BOQ Budget Amount",
    BOQ_Budget_Remaining_Amount: "BOQ_Budget_Remaining_Amount",
    Add_New_Line: "Add New Line",
    Edit_Line: "Edit Line",
    Cancel: "Cancel",
    organization: "organization",
    destinationTypeCode: "Destination Type Code",
    lineType: "Line Type",
    task: "Task",
    item: "Item",
    itemCategory: "item Category",
    unitCode: "Unit Code",
    unitPrice: "Unit Price",
    BOQQuantity: "BOQ Quantity",
    Editbtn: "Edit",
    Deletebtn: "Delete"
  },
  ar: {
    logout: "تسجيل الخروج",
    projects: "المشاريع",
    sign_in: "تسجيل الدخول",
    Email: "البريد الالكترونى",
    Password: "كلمة المرور",
    Please_input_password: "من فضلك ادخل كلمة المرور",
    Please_input_email_address: "من فضلك ادخل بريد الكتروني",
    Please_input_correct_email_address: "من فضلك ادخل بريد الكتروني صحيح",
    Congrats_you_sign_in_successful: "لقد تم تسجيل الدخول بنجاح",
    Clean_Data: "مسح",
    Project_Name: "اسم المشروع",
    Project_Number: "رقم المشروع",
    Project_Describtion: "وصف المشروع",
    Create_BOQ: "انشاء فاتوره",
    BOQ_Version: "اصدرار الفاتوره",
    BOQ_Budget_Amount: "مبلغ ميزانية BOQ",
    BOQ_Budget_Remaining_Amount: "المبلغ المتبقي لميزانية BOQ",
    Add_New_Line: "إضافة خط جديد",
    Edit_Line: "تحرير الخط",
    Cancel: "إلغاء",
    organization: "المؤسسة",
    destinationTypeCode: "رمز نوع الوجهة",
    lineType: "نوع الخط",
    task: "مهمة",
    item: "عنصر",
    itemCategory: "العنصر فئة",
    unitCode: "رمز الوحدة",
    unitPrice: "سعر الوحدة",
    BOQQuantity: "كمية الكميات",
    Editbtn: "تعديل",
    Deletebtn: "حذف"
  }
};

const i18n = new VueI18n({
  locale: "en", // set locale
  fallbackLocale: "ar", // set fallback locale
  messages // set locale messages
});

export default i18n;
