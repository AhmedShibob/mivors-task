import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
    path: "",
    component: () => import("../layouts/main/Main.vue"),
    children: [
      {
        path: "/",
        name: "Home",
        component: () => import("../views/Home.vue"),
        beforeEnter: (to, from, next) => {
          if (store.state.authenticated) {
            next();
          } else {
            next("/Login");
          }
        }
      },
      {
        path: "/CreateBOQ/:productName",
        name: "Create-BOQ",
        component: () => import("../views/CreateBOQ/CreateBOQ.vue"),
        beforeEnter: (to, from, next) => {
          if (store.state.authenticated) {
            next();
          } else {
            next("/Login");
          }
        }
      }
    ]
  },
  {
    // =============================================================================
    // LOGIN LAYOUT ROUTES
    // =============================================================================
    path: "",
    component: () => import("../layouts/login/loginLayouts.vue"),
    children: [
      {
        path: "/Login",
        name: "Login",
        component: () => import("../views/Login.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes
});

export default router;
